ARG DISTRO=debian
ARG RELEASE=buster
ARG ARCH="$(uname -m)"
ARG TIMEZONE=America/New_York
ARG USER=doc
ARG UID=1000
ARG DOTFILES=https://gitlab.com/narvin/dotfiles.git

# Debian Buster packages, independent of architecture
FROM debian:buster as packages-debian-buster
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
RUN apt-get update \
  && apt-get -y install \
    curl=7.64.0-4+deb10u2 \
    git=1:2.20.1-2+deb10u3 \
    make=4.2.1-1.2 \
    pass=1.7.3-2 \
    sudo=1.8.27-1+deb10u3 \
    vim=2:8.1.0875-5 \
    zsh=5.7.1-1 \
  && rm -rf /var/lib/apt/lists/*

# Debian Buster packages for ARM32
FROM packages-debian-buster as packages-debian-buster-armv7l
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
RUN apt-get update \
  && apt-get -y install \
    libffi-dev=3.2.1-9 \
    python3-pip=18.1-5 \
  && (cd /tmp && curl -O https://download.docker.com/linux/debian/dists/buster/pool/stable/armhf/docker-ce-cli_20.10.6~3-0~debian-buster_armhf.deb) \
  && dpkg -i /tmp/docker-ce-cli_20.10.6~3-0~debian-buster_armhf.deb \
  && pip3 install --no-cache-dir docker-compose==1.29.1 \
  && apt-get -y remove libffi-dev \
  && rm -rf /var/lib/apt/lists/* /tmp/*

# Debian Buster packages for AMD64
FROM packages-debian-buster as packages-debian-buster-x86_64
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
WORKDIR /tmp
RUN curl -O https://download.docker.com/linux/debian/dists/buster/pool/stable/amd64/docker-ce-cli_20.10.6~3-0~debian-buster_amd64.deb \
  && dpkg -i docker-ce-cli_20.10.6~3-0~debian-buster_amd64.deb \
  && curl \
    -L https://github.com/docker/compose/releases/download/1.29.1/docker-compose-Linux-x86_64 \
    -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose \
  && rm -rf /var/lib/apt/lists/* /tmp/*

# Ubuntu Focal packages, independent of architecture
FROM ubuntu:focal as packages-ubuntu-focal
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
RUN apt-get update \
  && apt-get -y install \
    curl=7.68.0-1ubuntu2.5 \
    git=1:2.25.1-1ubuntu3.1 \
    make=4.2.1-1.2 \
    sudo=1.8.31-1ubuntu1.2 \
    vim=2:8.1.2269-1ubuntu5 \
    zsh=5.8-3ubuntu1 \
  && rm -rf /var/lib/apt/lists/*

# Ubuntu Focal packages for ARM32
FROM packages-ubuntu-focal as packages-ubuntu-focal-armv7l
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get -y install \
    libffi-dev=3.3-4 \
    libssl-dev=1.1.1f-1ubuntu2.3 \
    python3-pip=20.0.2-5ubuntu1.1 \
    rustc=1.47.0+dfsg1+llvm-1ubuntu1~20.04.1 \
  && (cd /tmp && curl -O https://download.docker.com/linux/ubuntu/dists/bionic/pool/stable/armhf/docker-ce-cli_20.10.6~3-0~ubuntu-bionic_armhf.deb) \
  && dpkg -i /tmp/docker-ce-cli_20.10.6~3-0~ubuntu-bionic_armhf.deb \
  && pip3 install --no-cache-dir docker-compose==1.29.1 \
  && apt-get -y remove libffi-dev libssl-dev rustc \
  && rm -rf /var/lib/apt/lists/* /tmp/*

# Ubuntu Focal packages for AMD64
FROM packages-ubuntu-focal as packages-ubuntu-focal-x86_64
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
WORKDIR /tmp
RUN curl -O https://download.docker.com/linux/ubuntu/dists/focal/pool/stable/amd64/docker-ce-cli_20.10.6~3-0~ubuntu-focal_amd64.deb \
  && dpkg -i docker-ce-cli_20.10.6~3-0~ubuntu-focal_amd64.deb \
  && curl \
    -L https://github.com/docker/compose/releases/download/1.29.1/docker-compose-Linux-x86_64 \
    -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose \
  && rm -rf /var/lib/apt/lists/* /tmp/*

# Target image that will be built based on the distro, release, and architecture
FROM packages-"${DISTRO}-${RELEASE}-${ARCH}" as main
LABEL maintainer="Narvin Singh <narvin@nmco.us>"
ARG DISTRO
ARG RELEASE
ARG TIMEZONE
ARG USER
ARG UID
ARG DOTFILES
ENV POWERLINE=0
ENV PS1_USER="${USER}"
ENV PS1_HOST="${DISTRO}-${RELEASE}"
RUN ln -sf /usr/share/zoneinfo/"${TIMEZONE}" /etc/localtime \
  && printf 'export TERM=xterm-256color\n' >> /etc/zsh/zshenv \
  && printf 'export ZDOTDIR=${HOME}/.config/zsh\n' >> /etc/zsh/zshenv \
  && printf 'export TERM=xterm-256color\n' >> /etc/bash.bashrc \
  && printf 'source ${HOME}/.config/bash/bashrc\n' >> /etc/bash.bashrc \
  && chmod a+s $(command -v docker) \
  && useradd -m -u "${UID}" -G sudo -s /bin/bash "${USER}" \
  && printf '%s ALL=(ALL) NOPASSWD:ALL\n' "${USER}" >> /etc/sudoers
USER "${USER}"
WORKDIR /home/"${USER}"
RUN git clone "${DOTFILES}" .config \
  && mkdir src \
  && git clone https://gitlab.com/narvin/lint.git ./src/lint \
  && (cd ./src/lint && sudo make) \
  && ln -s /var/.ssh .ssh \
  && ln -s /var/project project
ENTRYPOINT ["/bin/bash"]

