TARGET != uname -m
BUILD = DOCKER_BUILDKIT=1 docker build
TAG = docker tag
RMI = docker image rm

all: debian ubuntu

debian: buster

buster:
	$(BUILD) -t narvin/dev:buster-$(TARGET) \
		--build-arg DISTRO=debian \
		--build-arg RELEASE=buster \
		--build-arg ARCH=$(TARGET) \
		.
	$(TAG) narvin/dev:buster-$(TARGET) narvin/dev:buster

ubuntu: focal

focal:
	$(BUILD) -t narvin/dev:focal-$(TARGET) \
		--build-arg DISTRO=ubuntu \
		--build-arg RELEASE=focal \
		--build-arg ARCH=$(TARGET) \
		.
	$(TAG) narvin/dev:focal-$(TARGET) narvin/dev:focal

clean:
	$(RMI) narvin/dev:buster-$(TARGET)
	$(RMI) narvin/dev:focal-$(TARGET)
	$(RMI) narvin/dev:buster
	$(RMI) narvin/dev:focal

