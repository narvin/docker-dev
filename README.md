Docker Dev
==========

These are minimal development environments in docker containers. With them,
you can:

- install your dev tools, so the host stays clean
- mount the docker socket, so you can manage docker from within docker
- mount the user's ssh directory, so you can authenticate yourself
- mount project directories, so you can persist your work

You should **never use these containers in production** because they have root
access on the host. These containers are meant to be run on a development
machine where you have root or sudo access. Each container is a disposable
layer that sits on top of the host, where you can install whatever dev tools
you need and do pretty much anything you want, while leaving the host clean.

Requirements
------------

The supported OS/Architectures are Linux/AMD64 and Linux/ARM32.

Installation
------------

Build the development environment docker images locally.

```Shell
git clone https://gitlab.com/narvin/docker-dev.git
cd docker-dev
make
```

Usage
-----

Compose up Debian Buster and Ubuntu Focal dev environments with:

- `curl`, `docker-cli`, `docker-compose`, `git`, `make`, `sudo`, `vim`, and
    `zsh` installed with their dependencies
- the time zone set to `America/New_York`
- the terminal set to `xterm-256color`
- `bash` and `zsh` configured to source their config files from `~/.config`
- `docker` permissions set so any user can run it
- a user `doc` with a user ID of `1000`, and `sudo` access without a password
- my dotfiles repo cloned into `~/.config`
- `lint` installed from https://gitlab.com/narvin/lint.git
- access to the host docker socket so you can run docker in docker
- the user's `.ssh` directory bind mounted and symlinked in the container
    user's home directory
- a project directory bind mounted and symlinked in the container user's
    home directory

```Shell
docker-compose up -d
```

Exec into either container to begin using it.

```Shell
docker exec -it docker-dev_buster_1 /bin/bash
docker exec -it docker-dev_focal_1 /bin/bash
```

Execute `docker` and `docker-compose` commands inside either container.

```Shell
docker ps -a
docker run -it myimage
docker-compose up
```

Build an alternate image with a different user and user ID to match your
identity on the host, this way any files you create in the bind mounted
volume in the container will be owned by your user on the host. You can also
specify the distribution/release (`debain`/`buster` or `ubuntu`/`focal`),
and clone your own dotfiles repo into `~/.config`. Then run the container
with access to docker on the host, your ssh keys, and your source code.

```Shell
DOCKER_BUILDKIT=1 docker build \
    -t dev:me \
    --build-arg USER=me \
    --build-arg UID=1005 \
    --build-arg DISTRO=debian \
    --build-arg RELEASE=buster \
    --build-arg DOTFILES=https://gitlab.com/myrepo/dotfiles \
    .
docker run -it \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v ~/.ssh:/var/.ssh \
    -v /host/path/to/src:/var/project \
    dev:me
```

